import React, { useState } from 'react';
import Button from '../../UI/Button/Button';
import Input from '../../UI/Input/Input';
import './TodoForm.css';

function TodoForm({ onFormSubmitCallback, isFormEdited, todo = {} }) {
  const [titleValidationLabel, setTitleValidationLabel] = useState('');
  const [descValidationLabel, setDescValidationLabel] = useState('');

  const onFormSubmit = event => {
    event.preventDefault();

    const inputs = event.target.elements;
    const newTodoTitle = inputs[0].value;
    const newTodoDesc = inputs[1].value;
    let newTodoPrior = inputs[2].value;

    let isFormInvalid = false;

    if (newTodoTitle.length < 5) {
      setTitleValidationLabel('Provide a longer title! (minimum 5)')
      isFormInvalid = true;
    } else {
      setTitleValidationLabel('')
    }

    if (newTodoDesc.length < 5) {
      setDescValidationLabel('Provide a longer description! (minimum 5)')
      isFormInvalid = true;
    } else {
      setDescValidationLabel('')
    }

    // 4# Dodaj walidację autora oraz URL
    // -------------------------------

    if (isFormInvalid) return;

    const todoObject = {
      title: newTodoTitle,
      description: newTodoDesc,
      priority: newTodoPrior ? newTodoPrior : '0',
    }

    if (isFormEdited) {
      todoObject.id = todo.id;
    }

    onFormSubmitCallback(todoObject)

    for (const input of inputs) {
      input.value = "";
    }
  }

  return (
    <section>
      <h1>{isFormEdited ? 'Edit task' : 'Create your task'}</h1>
      <form onSubmit={onFormSubmit}>
        <Input
          placeholder="Go out with a dog"
          label="Provide a task name"
          id="taskTitle"
          name="taskTitle"
          validationError={titleValidationLabel}
          value={todo.title}
        />
        <Input
          placeholder="Do it after it eats"
          label="Provide a task description"
          id="taskDesc"
          name="taskDesc"
          validationError={descValidationLabel}
          value={todo.description}
        />
        <Input
          placeholder="0"
          label="Provide a task priority"
          id="taskPriority"
          name="taskPriority"
          type="number"
          value={todo.priority}
        />
        <Button type="submit" variant="add" size={2} label={isFormEdited ? 'SAVE' : 'ADD'} />
      </form>
    </section>
  );
}

export default TodoForm;
