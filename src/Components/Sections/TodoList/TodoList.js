import React from 'react';
import Button from '../../UI/Button/Button';
import './TodoList.css';

function TodoList({ todos, onTodoDelete, onTodoEdit }) {
  return (
    // 1# Przenieść style odpowiedzialne na listę do pliku CSS tego komponentu!

    <section>
      <h1>List of tasks:</h1>
      {todos.map(todo => {
        let style = {}

        if (todo.priority === 1) {
          style = {
            border: '1px solid blue'
          }
        } else if (todo.priority === 2) {
          style = {
            border: '1px solid red'
          }
        } else {
          style = {
            border: '1px solid gray'
          }
        }

        // 2# Przerobić z inline stylów na podejście klasowe
        // Przykład w Button.js

        return (
          <div className="todo" key={todo.id} style={style}>
            <h2>Title: {todo.title}</h2>
            <p>Description: {todo.description}</p>
            <Button
              label="Delete"
              callbackFn={() => {onTodoDelete(todo.id)}}
              variant="delete"
            />

            <Button
              label="Edit"
              callbackFn={() => {onTodoEdit(todo)}}
              variant="edit"
            />
          </div>
        )
      })}
    </section>
  );
}

export default TodoList;
