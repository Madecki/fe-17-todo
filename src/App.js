import React, { useEffect, useState } from 'react';
import { getTodos, deleteTodo, addTodo, editTodo } from './Requests';
import './App.css';
import TodoList from './Components/Sections/TodoList/TodoList';
import TodoForm from './Components/Sections/TodoForm/TodoForm';
import Modal from './Components/UI/Modal/Modal';

function App() {
  const [todos, updateTodos] = useState([]);
  const [editedTodo, setEditedTodo] = useState(false);
  const [didRequestFail, setRequestStatus] = useState(false);

  useEffect(() => {
    getAndRenderTodos()
  }, [])

  const getAndRenderTodos = () => {
    getTodos().then(response => {
      const { data } = response;
      updateTodos(data)
    });
    // 3# Dodać obsługę błędów -> jezeli request zwróci błąd, to wyświetl go w widoku
  }

  const onTodoDelete = id => {
    deleteTodo(id).then(() => {
      getAndRenderTodos();
    });
  }

  const onTaskAdd = todo => {
    addTodo(todo).then(() => {
      getAndRenderTodos();
    });
  }

  const onTaskEdit = todo => {
    console.log(todo);
    editTodo(todo).then(() => {
      getAndRenderTodos();
      setEditedTodo(false);
    })
  }

  const openEditModal = todo => {
    setEditedTodo(todo);
  }

  return (
    <main>
      <div className="container">
        { didRequestFail && <p>Sorry I couldn't download the data. Please try again later.</p> }
        { !didRequestFail && (
          <>
            <TodoForm onFormSubmitCallback={onTaskAdd} />
            <TodoList todos={todos} onTodoDelete={onTodoDelete} onTodoEdit={openEditModal} />
          </>
        ) }

        { editedTodo && (
          <Modal>
            <TodoForm onFormSubmitCallback={onTaskEdit} isFormEdited todo={editedTodo} />
          </Modal>
        )}
      </div>
    </main>
  );
}

export default App;
